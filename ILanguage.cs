﻿using Lab2;

namespace LAB2
{
	public interface ILanguage
	{
		void DisplayChoice();
		void DisplayFunctionNames(IFunction funcData);
		void BadChoiceComunicate();
		void DisplayRanges();
	}
}
