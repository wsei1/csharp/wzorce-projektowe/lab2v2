﻿using Lab2;
using System;

namespace LAB2
{
	public class PolishUI : ILanguage
	{
		public void BadChoiceComunicate()
		{
			Console.WriteLine("Wpisales niepoprawna liczbe!");
		}

		public void DisplayChoice()
		{
			Console.WriteLine("Wybierz dla której funkcji, chcesz liczyć całkę.");
			Console.WriteLine("Dostępne funkcje:");
		}

		public void DisplayFunctionNames(IFunction funcData)
		{
			Console.WriteLine("ID: {0}, Nazwa: {1}", funcData.Id, funcData.Name);
		}

		public void DisplayRanges()
		{
			Console.WriteLine("Dla ktorego przedzialu chcesz policzyc całkę?");
			Console.WriteLine("Przedzial 1: od -10 do 10");
			Console.WriteLine("Przedzial 2: od -5 do 20");
			Console.WriteLine("Przedzial 3: od -5 do 0");
		}
	}
}
