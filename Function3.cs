﻿namespace Lab2
{
	class Function3 : IFunction
    {
        public int Id => 3;

        public string Name => "y=2x - 3";

        public decimal GetY(decimal x)
        {
            return 2 * x - 3;
        }
    }
}
