﻿namespace LAB2
{
	public class UIFactory
	{
		public static ILanguage Create(TypeLanguage type)
		{
			ILanguage language = null;
			switch (type)
			{
				case TypeLanguage.PolishUI:
					language = new PolishUI();
					break;
				case TypeLanguage.EnglishUI:
					language = new EnglishUI();
					break;
			}
			return language;
		}
	}
}
