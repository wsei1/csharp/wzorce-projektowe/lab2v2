﻿namespace Lab2
{
	interface ICalculator
    {
        string Name { get; }
        string EnglishName { get; }
        decimal GetIntegralValue(IFunction function, decimal rangeFrom, decimal rangeTo);
    }
}
