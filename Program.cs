﻿using LAB2;
using System;
using System.Collections.Generic;
using System.Linq;

/* 
 Mateusz Domagała
 Wzorce Projektowe
*/

namespace Lab2
{
	class Program
	{
		static void Main(string[] args)
		{
			int minValue = 0;
			int maxValue = 0;
			List<IFunction> Functions = new List<IFunction>
			{
				new Function1(),
				new Function2(),
				new Function3(),
				new Function4()
			};

			List<ICalculator> Calculators = new List<ICalculator>
			{
				new Trapeze(),
				new Rectangle()
			};

			Console.WriteLine("Wybierz język: ");
			Console.WriteLine("1. Polski");
			Console.WriteLine("2. Angielski");
			var languageChoice = int.Parse(Console.ReadLine());

			ILanguage language = null;

			switch(languageChoice)
			{
				case 1:
					language = UIFactory.Create(TypeLanguage.PolishUI);
					break;
				case 2:
					language = UIFactory.Create(TypeLanguage.EnglishUI);
					break;
				default:
					Console.WriteLine("Zły wybór!");
					break;
			}

			language.DisplayChoice();

			foreach (IFunction funcData in Functions)
			{
				language.DisplayFunctionNames(funcData);
			}

			var choose = int.Parse(Console.ReadLine());

			if (choose > Functions.Count + 1 || choose == 0)
			{
				language.BadChoiceComunicate();
				Console.ReadKey();
				return;
			}

			language.DisplayRanges();
			
			var rangeChoose = int.Parse(Console.ReadLine());

			switch (rangeChoose)
			{
				case 1:
					minValue = -10;
					maxValue = 10;
					break;
				case 2:
					minValue = -5;
					maxValue = 20;
					break;
				case 3:
					minValue = -5;
					maxValue = 0;
					break;
				default:
					language.BadChoiceComunicate();
					Console.ReadKey();
					return;
			}

			Calculate(Calculators, Functions, choose, minValue, maxValue, languageChoice);
			
			Console.ReadKey();
		}

		private static void Calculate(List<ICalculator> Calculators, List<IFunction> functions, int choose, int minValue, int maxValue, int languageChoice)
		{
			foreach (var calculator in Calculators)
			{
				if (languageChoice == 1)
				{
					Console.WriteLine(calculator.Name);
				} else
				{
					Console.WriteLine(calculator.EnglishName);
				}
				Console.WriteLine(calculator.GetIntegralValue(functions.First(x => x.Id == choose), minValue, maxValue));
			}
		}
	}
}
