﻿using Lab2;
using System;

namespace LAB2
{
	public class EnglishUI : ILanguage
	{
		public void BadChoiceComunicate()
		{
			Console.WriteLine("You have choosen wrong number.");
		}

		public void DisplayChoice()
		{
			Console.WriteLine("Choose which function you want to use to calculate integral.");
			Console.WriteLine("Available functions:");
		}

		public void DisplayFunctionNames(IFunction funcData)
		{
			Console.WriteLine("ID: {0}, Name: {1}", funcData.Id, funcData.Name);
		}

		public void DisplayRanges()
		{
			Console.WriteLine("Which range you want to use to calculate integral?");
			Console.WriteLine("Range 1: from -10 to 10");
			Console.WriteLine("Range 2: from -5 to 20");
			Console.WriteLine("Range 3: from -5 to 0");
		}
	}
}
